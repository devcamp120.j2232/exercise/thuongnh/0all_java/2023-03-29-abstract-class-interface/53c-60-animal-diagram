public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo 1 đối tượng con mèo 
        Cat cat = new Cat("tom");
        // khởi tạo đối tượng con chó
        Dog dog  = new Dog("jons");
        // khởi tạo đối tượng con chó to
        BigDog bigDog = new BigDog("chó lớn ");

        // in ra tiếng kêu của 3 đối tượng ttrên 
        cat.Greets();
        dog.Greets();
        dog.Greets(dog);

        bigDog.Greets();
        bigDog.Greets(dog);
        bigDog.Greets(bigDog);

    }
}

public class BigDog extends Dog {
    
    // phương thức khởi tạo đối tượng big dog
    public BigDog(String name){
        super(name);
    }

    // phương thức chào hỏi của đối tượng 
    public void Greets(){
        System.out.println("wooow");
    }

    // phuong thức chào hỏi củacon chó lớn với 1 con chó nhỏ 
    public void Greets(Dog another){
        System.out.println("woooooooow");
    }
    // phuong thức chào hỏi củacon chó lớn với 1 con chó lớn
    public void Greets(BigDog another){
        System.out.println("woooooooooooooooooow");
    }


}

public class Dog extends Animal {
    public Dog(String name){
        super(name);
    }

    public void Greets() {
        System.out.println("Woof");
    }

    public void Greets(Dog another) {
        System.out.println("Wooooof");
    }
    
}

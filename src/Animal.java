public abstract class  Animal {

    // lớp động vật  <<abstract>>
    private String name;
    // phương thức khoi tạo
    public Animal(String name) {
        this.name = name;
    }
    // phương thức trừu tượng Greets
    public abstract void Greets();
}
